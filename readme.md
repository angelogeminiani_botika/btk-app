# BTK App #

This package contains a stub for Applications.



SAMPLE CUSTOMIZATION:

```go
func TestApp(t *testing.T) {
    // init environment (ONLY FOR TEST)
    gg.Paths.SetWorkspacePath("./_workspace")
    gg.Paths.SetTempRoot(gg.Paths.WorkspacePath("/temp"))
    
    // creates application
    app, err := btkapp.NewApplication("debug")
    if nil != err {
        t.Error(err)
        t.FailNow()
    }
    
    // override default settings and ensure http and database are enabled
    app.Settings().Serve.Webserver = true
    app.Settings().Serve.Database = true
    err = app.SaveSettings()
    if nil != err {
        t.Error(err)
        t.FailNow()
    }
    
    // expose API
    app.Http().RegisterNoAuth("get", "/api/v1/cmd/custom", custom.CmdCustom) // no http authentication
    app.Http().RegisterAuth("get", "/api/v1/cmd/custom_auth", custom.CmdCustom) // http authentication
    // add custom handlers
    app.Command().SetExecutor(custom.NewExecCustom())
    
    err = app.Start()
    if nil != err {
        t.Error(err)
        t.FailNow()
    }
    
    // wait forever
    app.Join()
}
```

START command parameters
```
run -m=debug -dir_work=./_test/_workspace
```

## DEFAULT API DOCUMENTATION ##

 - [Json export file from Postman](./_docs/GGX-APP.postman_collection.json)