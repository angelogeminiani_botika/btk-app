package btkweb

import (
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
)

type BtkWebcontroller struct {
	app *ggxapp.GGxApplicationRuntime
}

func NewBtkWebcontroller(app *ggxapp.GGxApplicationRuntime) (instance *BtkWebcontroller) {
	instance = new(BtkWebcontroller)
	instance.app = app

	instance.init()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *BtkWebcontroller) init() {

	//-- company API --//
	instance.app.Webserver().RegisterAuth("post", btkcommons.ApiAccountCompanyAssert, btkcommons.CmdAccountCompanyAssert)
	instance.app.Webserver().RegisterAuth("post", btkcommons.ApiAccountCompanyList, btkcommons.CmdAccountCompanyList)

	//-- util --//
	instance.app.Webserver().RegisterAuth("post", btkcommons.ApiUtilGetValue, btkcommons.CmdUtilGetValue)
}
