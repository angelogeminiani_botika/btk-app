package btkexecutors

import (
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
)

type BtkExecutors struct {
	app *ggxapp.GGxApplicationRuntime
}

func NewBtkExecutors(app *ggxapp.GGxApplicationRuntime) (instance *BtkExecutors) {
	instance = new(BtkExecutors)
	instance.app = app

	instance.init()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *BtkExecutors) init() {
	// sample
	instance.app.Command().SetExecutor(NewExecCompany())
	instance.app.Command().SetExecutor(NewExecUtil())

}
