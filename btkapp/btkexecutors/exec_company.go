package btkexecutors

import (
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkcommons"
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkdatabase/btkquery"
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcontroller/ggxcommand"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database/model"
	"fmt"
)

const (
	CmdAccountCompanyAssert = btkcommons.CmdAccountCompanyAssert
	CmdAccountCompanyList   = btkcommons.CmdAccountCompanyList
)

type ExecCompany struct {
	ctrl *ggxcommand.GGxCommands
}

func NewExecCompany() (instance *ExecCompany) {
	instance = new(ExecCompany)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ExecCompany) Close() {}

func (instance *ExecCompany) SetController(ctrl *ggxcommand.GGxCommands) ggxcommand.IExecutor {
	instance.ctrl = ctrl
	return instance
}

func (instance *ExecCompany) IsHandler(commandName string) bool {
	return gg.Arrays.IndexOf(commandName, []string{
		CmdAccountCompanyAssert,
		CmdAccountCompanyList,
	}) > -1
}

func (instance *ExecCompany) Execute(sender *model.User, command *ggxcommand.ApiStatement) (response interface{}, err error) {
	switch command.Statement {
	case CmdAccountCompanyAssert:
		return instance.accountCompanyAssert(command.Params)
	case CmdAccountCompanyList:
		return instance.accountCompanyList(command.Params)
	}
	return nil, gg.Errors.Prefix(ggxcommons.ExecutionError,
		fmt.Sprintf("Command not supported: '%s'", command.Statement))
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ExecCompany) query() *btkquery.CompanyQuery {
	if nil != instance && nil != instance.ctrl.Database() {
		if instance.ctrl.Database().HasQueryController(btkquery.NameCompanyQuery) {
			return instance.ctrl.Database().GetQueryController(btkquery.NameCompanyQuery).(*btkquery.CompanyQuery)
		}
	}
	return nil
}

func (instance *ExecCompany) accountCompanyAssert(params map[string]interface{}) (response interface{}, err error) {
	userId := gg.Reflect.GetInt(params, "user_id")
	name := gg.Reflect.GetString(params, "name")
	vat := gg.Reflect.GetString(params, "vat")
	if userId == 0 {
		err = gg.Errors.Prefix(ggxapp.MissingParamsError, "user_id is required")
		return
	}
	q := instance.query()
	company, qe := q.CompanyGetOrCreateByUserId(uint(userId), name, vat)
	if nil != qe {
		err = qe
		return
	}
	response = company.ToMap()

	return
}

func (instance *ExecCompany) accountCompanyList(params map[string]interface{}) (response interface{}, err error) {
	list := make([]map[string]interface{}, 0)
	q := instance.query()
	companies, e := q.CompanyAll()
	if nil != e {
		err = e
		return
	}
	for _, company := range companies {
		list = append(list, company.ToMapLite())
	}
	response = list

	return
}
