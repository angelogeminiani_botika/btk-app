package btkexecutors

import (
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkcommons"
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkdatabase/btkquery"
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcontroller/ggxcommand"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database/model"
	"fmt"
)

const (
	CmdUtilGetValue = btkcommons.CmdUtilGetValue
)

type ExecUtil struct {
	ctrl *ggxcommand.GGxCommands
}

func NewExecUtil() (instance *ExecUtil) {
	instance = new(ExecUtil)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ExecUtil) Close() {}

func (instance *ExecUtil) SetController(ctrl *ggxcommand.GGxCommands) ggxcommand.IExecutor {
	instance.ctrl = ctrl
	return instance
}

func (instance *ExecUtil) IsHandler(commandName string) bool {
	return gg.Arrays.IndexOf(commandName, []string{
		CmdUtilGetValue,
	}) > -1
}

func (instance *ExecUtil) Execute(sender *model.User, command *ggxcommand.ApiStatement) (response interface{}, err error) {
	switch command.Statement {
	case CmdUtilGetValue:
		return instance.utilGetValue(command.Params)

	}
	return nil, gg.Errors.Prefix(ggxcommons.ExecutionError,
		fmt.Sprintf("Command not supported: '%s'", command.Statement))
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ExecUtil) query() *btkquery.CompanyQuery {
	if nil != instance && nil != instance.ctrl.Database() {
		if instance.ctrl.Database().HasQueryController(btkquery.NameCompanyQuery) {
			return instance.ctrl.Database().GetQueryController(btkquery.NameCompanyQuery).(*btkquery.CompanyQuery)
		}
	}
	return nil
}

func (instance *ExecUtil) utilGetValue(params map[string]interface{}) (response interface{}, err error) {
	name := gg.Reflect.GetString(params, "name")
	if len(name) > 0 {
		response = map[string]interface{}{
			"value": "12345",
		}
	} else {
		err = gg.Errors.Prefix(ggxapp.MissingParamsError, "'name' param is required!")
	}

	return
}
