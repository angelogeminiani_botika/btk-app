package btkmodel

import "gorm.io/gorm"

// CompanyUser
// Relations between a company and many users
type CompanyUser struct {
	gorm.Model
	CompanyId uint `gorm:"index:idx_company_user"`
	UserId    uint `gorm:"index:idx_company_user"`
}
