package btkmodel

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	"gorm.io/gorm"
	"strings"
)

// Company
// Companies model. A user can be connected to a company
// Companies can be connected to many users. Each company has a database
type Company struct {
	gorm.Model
	Uid     string // unique id used as path for company root
	Slug    string
	Name    string
	Vat     string `gorm:"index:idx_company_vat,unique"`
	Address string
	AdminId uint // user_id
}

func (instance *Company) BeforeSave(_ *gorm.DB) (err error) {
	if len(instance.Name) > 0 {
		instance.Slug = ggxapp.Slugify(instance.Name)
	}
	if len(instance.Uid) == 0 {
		instance.Uid = strings.ToLower(ggxapp.MD5(gg.Rnd.Uuid()))
	}
	return
}

func (instance *Company) BeforeUpdate(_ *gorm.DB) (err error) {
	if len(instance.Name) > 0 {
		instance.Slug = ggxapp.Slugify(instance.Name)
	}
	if len(instance.Uid) == 0 {
		instance.Uid = strings.ToLower(ggxapp.MD5(gg.Rnd.Uuid()))
	}
	return
}

func (instance *Company) ToMap() (response map[string]interface{}) {
	response = make(map[string]interface{})
	response["id"] = instance.ID
	response["uid"] = instance.Uid
	response["slug"] = instance.Slug
	response["name"] = instance.Name
	response["vat"] = instance.Vat
	response["address"] = instance.Address
	response["admin_id"] = instance.AdminId
	response["created_at"] = gg.Dates.FormatDate(instance.CreatedAt, "MM-dd-yyyy HH:mm")
	response["created_timestamp"] = 1000 * instance.CreatedAt.Unix()

	return
}

func (instance *Company) ToMapLite() (response map[string]interface{}) {
	response = make(map[string]interface{})
	response["uid"] = instance.Uid
	response["created_at"] = gg.Dates.FormatDate(instance.CreatedAt, "MM-dd-yyyy HH:mm")
	response["created_timestamp"] = 1000 * instance.CreatedAt.Unix()

	return
}
