package btkdatabase

import (
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkcommons"
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkdatabase/btkmodel"
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkdatabase/btkquery"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
)

type BtkDatabase struct {
	app *ggxapp.GGxApplicationRuntime
}

func NewBtkDatabase(app *ggxapp.GGxApplicationRuntime) (instance *BtkDatabase, err error) {
	instance = new(BtkDatabase)
	instance.app = app

	err = instance.init()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *BtkDatabase) init() (err error) {
	err = instance.initSchemas()
	if nil != err {
		return

	}

	instance.initControllers()

	return
}

func (instance *BtkDatabase) initSchemas() (err error) {
	schemas := make(map[string][]interface{})

	schemas[btkcommons.DbMain] = []interface{}{
		&btkmodel.Company{},
		&btkmodel.CompanyUser{},
	}

	err = instance.app.Database().AutoMigrateSchema(schemas)

	return
}

func (instance *BtkDatabase) initControllers() {
	// register query controllers
	instance.app.Database().SetQueryController(btkquery.NameCompanyQuery, btkquery.NewCompanyQuery)

	return
}
