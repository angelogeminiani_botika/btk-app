package btkquery

import (
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkcommons"
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkdatabase/btkmodel"
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database_pool"
	"gorm.io/gorm"
)

const NameCompanyQuery = "query_company"

type CompanyQuery struct {
	pool *ggxserve_database_pool.Connections
}

func NewCompanyQuery(pool *ggxserve_database_pool.Connections) (response interface{}, err error) {
	instance := new(CompanyQuery)
	instance.pool = pool
	err = instance.init()
	response = instance
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *CompanyQuery) CompanyGetById(id uint) (response *btkmodel.Company, err error) {
	if nil != instance {
		response, err = instance.getCompany("id=?", id)
	}
	return
}

func (instance *CompanyQuery) CompanyGetByVat(vat string) (response *btkmodel.Company, err error) {
	if nil != instance {
		response, err = instance.getCompany("vat=?", vat)
	}
	return
}

func (instance *CompanyQuery) CompanyGetByName(name string) (response *btkmodel.Company, err error) {
	if nil != instance {
		response, err = instance.getCompany("slug=?", ggxapp.Slugify(name))
	}
	return
}

func (instance *CompanyQuery) CompanyGetBySlug(slug string) (response *btkmodel.Company, err error) {
	if nil != instance {
		response, err = instance.getCompany("slug=?", slug)
	}
	return
}

func (instance *CompanyQuery) CompanyGetOrCreateByUserId(userId uint, name, vat string) (response *btkmodel.Company, err error) {
	if nil != instance {
		db, e := instance.db()
		if nil != e {
			err = e
			return
		}
		var tx *gorm.DB

		// try with company admin
		tx = db.Where("admin_id=?", userId).First(&response)
		if nil != response && response.ID > 0 {
			return
		}

		// try to check if this user is already a company user
		var companyUser btkmodel.CompanyUser
		tx = db.Model(&btkmodel.CompanyUser{}).Where("user_id=?", userId).First(&companyUser)
		if companyUser.ID > 0 {
			tx = db.Where("company_id=?", companyUser.ID).First(&response)
			if nil != response && response.ID > 0 {
				return
			}
		}

		// try to see if company exists
		tx = db.Where("vat=?", vat).First(&response)
		if nil != response && response.ID > 0 {
			// company exists, now create association with user
			companyUser = btkmodel.CompanyUser{
				CompanyId: response.ID,
				UserId:    userId,
			}
			tx = db.Create(&companyUser)
			if nil != tx.Error {
				err = gg.Errors.Prefix(tx.Error, "Creating item into collection 'company_users'")
				return
			}
		} else {
			// company does not exist
			// must create company with admin user and association
			response = &btkmodel.Company{
				Name:    name,
				Vat:     vat,
				Address: "",
				AdminId: userId,
			}
			tx = db.Create(response)
			if nil != tx.Error {
				err = gg.Errors.Prefix(tx.Error, "Creating item into collection 'company'")
				return
			}
			// creates association
			companyUser = btkmodel.CompanyUser{
				CompanyId: response.ID,
				UserId:    userId,
			}
			tx = db.Create(&companyUser)
			if nil != tx.Error {
				err = gg.Errors.Prefix(tx.Error, "Creating item into collection 'company_users'")
				return
			}
		}
	}
	return
}

func (instance *CompanyQuery) CompanySave(item *btkmodel.Company) (response *btkmodel.Company, err error) {
	if nil != instance && nil != item {
		db, e := instance.db()
		if nil != e {
			err = e
			return
		}
		var tx *gorm.DB
		if item.ID == 0 {
			tx = db.Create(item)
		} else {
			tx = db.Save(item)
		}
		if nil != tx.Error {
			err = gg.Errors.Prefix(tx.Error, "Saving item to collection 'companies'")
		}
		response = item
	}
	return
}

func (instance *CompanyQuery) CompanyAll() (response []*btkmodel.Company, err error) {
	if nil != instance {
		db, e := instance.db()
		if nil != e {
			err = e
			return
		}
		tx := db.First(&response)
		if nil != tx.Error {
			err = gg.Errors.Prefix(tx.Error, "Retrieving item from collection 'companies'")
			return
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *CompanyQuery) init() (err error) {

	return
}

func (instance *CompanyQuery) db() (db *gorm.DB, err error) {
	_, db, err = instance.pool.Get(btkcommons.DbMain)
	return
}

func (instance *CompanyQuery) getCompany(query string, values ...interface{}) (response *btkmodel.Company, err error) {
	db, e := instance.db()
	if nil != e {
		err = e
		return
	}
	tx := db.Where(query, values...).First(&response)
	if nil != tx.Error && !ggxapp.IsRecordNotFoundError(tx.Error) {
		err = gg.Errors.Prefix(tx.Error, "Retrieving item from collection 'companies'")
	}
	return
}
