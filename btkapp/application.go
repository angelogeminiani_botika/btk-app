package btkapp

import (
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkdatabase"
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkexecutors"
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkinitializer"
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp/btkweb"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxconfig"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcontroller/ggxcommand"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_webserver"
)

//----------------------------------------------------------------------------------------------------------------------
//	application
//----------------------------------------------------------------------------------------------------------------------

type BtkApplication struct {
	builder     *ggxapp.GGxApplicationBuilder
	app         *ggxapp.GGxApplicationRuntime
	initializer *btkinitializer.BtkInitializer

	executors     *btkexecutors.BtkExecutors
	database      *btkdatabase.BtkDatabase
	webcontroller *btkweb.BtkWebcontroller
}

func NewApplication(mode, dirWork string) (instance *BtkApplication, err error) {
	instance = new(BtkApplication)

	// initialize settings files
	err = instance.init(mode, dirWork)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *BtkApplication) Start() (err error) {
	if nil != instance && nil != instance.app {
		return instance.app.Start()
	}
	return
}

func (instance *BtkApplication) Stop() {
	if nil != instance && nil != instance.app {
		instance.app.Stop()
	}
}

func (instance *BtkApplication) Join() {
	if nil != instance && nil != instance.app {
		instance.app.Join()
	}
}

func (instance *BtkApplication) Settings() *ggxconfig.Settings {
	if nil != instance && nil != instance.builder {
		return instance.builder.Settings()
	}
	return new(ggxconfig.Settings)
}

func (instance *BtkApplication) SaveSettings() (err error) {
	if nil != instance && nil != instance.builder {
		err = instance.builder.SaveSettings()
	}
	return
}

func (instance *BtkApplication) Webserver() *ggxserve_webserver.GGxWebController {
	if nil != instance && nil != instance.app {
		return instance.app.Webserver()
	}
	return nil
}

func (instance *BtkApplication) Database() *ggxserve_database.GGxControllerDatabase {
	if nil != instance && nil != instance.app {
		return instance.app.Database()
	}
	return nil
}

func (instance *BtkApplication) Command() *ggxcommand.GGxCommands {
	if nil != instance && nil != instance.app {
		return instance.app.Command()
	}
	return nil
}

func (instance *BtkApplication) Events() *gg_events.Emitter {
	if nil != instance && nil != instance.app {
		return instance.app.Events()
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *BtkApplication) init(mode, dirWork string) (err error) {
	// creates the application builder
	instance.builder, err = ggxapp.NewApplicationBuilder(mode, dirWork)
	if nil != err {
		return
	}

	// override default settings and ensure http and database are enabled
	instance.builder.Settings().Serve.Webserver = true
	instance.builder.Settings().Serve.Database = true
	instance.builder.Settings().Serve.Templates = true
	err = instance.builder.SaveSettings()
	if nil != err {
		return
	}

	// INIT: initialize settings files
	instance.initializer = btkinitializer.NewBtkInitializer(instance.builder)
	err = instance.initializer.Initialize(mode)
	if nil != err {
		return
	}

	// BUILD: creates an application runtime
	instance.app, err = instance.builder.Build()
	if nil != err {
		return
	}

	instance.executors = btkexecutors.NewBtkExecutors(instance.app)
	instance.database, err = btkdatabase.NewBtkDatabase(instance.app)
	if nil != err {
		return
	}
	instance.webcontroller = btkweb.NewBtkWebcontroller(instance.app)

	return
}
