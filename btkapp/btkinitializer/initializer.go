package btkinitializer

import (
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	_ "embed"
	"fmt"
)

// ---------------------------------------------------------------------------------------------------------------------
//	s e t t i n g s
// ---------------------------------------------------------------------------------------------------------------------

//go:embed tpl_mysettings.json
var TplMySettings string

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

type BtkInitializer struct {
	app *ggxapp.GGxApplicationBuilder
}

func NewBtkInitializer(app *ggxapp.GGxApplicationBuilder) (instance *BtkInitializer) {
	instance = new(BtkInitializer)
	instance.app = app

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *BtkInitializer) Initialize(mode string) (err error) {

	// mysettings.mode.json
	err = instance.app.Initializer().DeployIntoWorkspace(fmt.Sprintf("mysettings.%s.json", mode), TplMySettings)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
