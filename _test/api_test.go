package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpclient"
	"fmt"
	"testing"
)

const (
	host = "http://localhost:9090/api/v1/"
)

func TestApi(t *testing.T) {
	accessToken, err := SignIn()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	pool := gg.Async.NewConcurrentPool(500)
	var i int
	for i = 0; i < 5000; i++ {
		_ = pool.RunArgs(func(args ...interface{}) error {
			token := args[0].(string)
			count := args[1].(int)
			value, e := UtilGetValue(gg.Convert.ToString(token))
			fmt.Println(count, gg.JSON.Stringify(value))
			return e
		}, accessToken, i)
	}
	poolErr := pool.Wait()
	if nil != poolErr.ErrorOrNil() {
		t.Error(i, poolErr.ErrorOrNil())
		t.FailNow()
	}

	fmt.Println("Task done: ", i)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func SignIn() (interface{}, error) {
	client := httpclient.NewHttpClient()
	response, err := client.Post(host+"auth/sign-in", map[string]interface{}{
		"email":    "angelo.geminiani@gmail.com",
		"password": "123456",
	})
	if nil != err {
		return nil, err
	}

	m := response.BodyAsMap()
	accessToken := gg.Maps.GetString(m, "response.auth.access_token")
	return accessToken, nil
}

func UtilGetValue(accessToken string) (interface{}, error) {
	client := httpclient.NewHttpClient()
	client.AddHeader("Authorization", "Bearer "+accessToken)
	response, err := client.Post(host+"util/get-value", map[string]interface{}{
		"name": "123",
	})
	if nil != err {
		return nil, err
	}
	m := response.BodyAsMap()
	return gg.Maps.Get(m, "response"), nil
}
