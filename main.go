package main

import (
	"bitbucket.org/angelogeminiani_botika/btk-app/btkapp"
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	"flag"
	_ "github.com/gofiber/fiber/v2"
	_ "gorm.io/driver/mysql"
	_ "gorm.io/driver/postgres"
	_ "gorm.io/driver/sqlite"
	_ "gorm.io/driver/sqlserver"
	_ "gorm.io/gorm"
	"log"
	"os"
)

func init() {
	ggxapp.SetAppName("My Custom App")
	ggxapp.SetAppVersion("0.0.1")
	ggxapp.SetAppSalt("") // encrypt sensible data
}

// Program arguments: run -dir_work=./test/_workspace
func main() {

	// PANIC RECOVERY
	defer ggxapp.Recover("main")

	//-- command flags --//
	// run
	cmdRun := flag.NewFlagSet("run", flag.ExitOnError)
	dirWork := cmdRun.String("dir_work", gg.Paths.Absolute("./_workspace"), "Set a particular folder as main workspace")
	mode := cmdRun.String("m", ggxapp.ModeProduction, "Mode allowed: 'debug' or 'production'")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "run":
			_ = cmdRun.Parse(os.Args[2:])
		default:
			panic("Command not supported: " + cmd)
		}
	} else {
		panic("Missing command. i.e. 'run'")
	}

	app, err := btkapp.NewApplication(*mode, *dirWork)
	if nil == err {

		err = app.Start()
		if nil != err {
			log.Panicf("Error starting %s: %s", ggxapp.GetAppName(), err.Error())
		} else {
			// app is running
			app.Join()
		}

	} else {
		log.Panicf("Error launching %s: %s", ggxapp.GetAppName(), err.Error())
	}

}
