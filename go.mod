module bitbucket.org/angelogeminiani_botika/btk-app

go 1.16

require (
	bitbucket.org/digi-sense/gg-core v0.1.60
	bitbucket.org/digi-sense/gg-xapp v0.1.30
	github.com/gofiber/fiber/v2 v2.31.0
	gorm.io/driver/mysql v1.3.3
	gorm.io/driver/postgres v1.3.4
	gorm.io/driver/sqlite v1.3.1
	gorm.io/driver/sqlserver v1.3.2
	gorm.io/gorm v1.23.4
)
